#language: pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "richard@gmail.com" e "123"
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentar logar

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input       | senha_input | mensagem_output                  |
            | richard@gmail.com | a123        | Usuário e/ou senha inválidos.    |
            | richard@404.com   | 123         | Usuário e/ou senha inválidos.    |
            | richard&aol.com   | a123        | Oops. Informe um email válido!   |
            |                   | a123        | Oops. Informe um email válido!   |
            | richard@gmail.com |             | Oops. Informe sua senha secreta! |






